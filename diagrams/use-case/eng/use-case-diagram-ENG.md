```plantuml
@startuml
skinparam actorStyle awesome

skinparam actor {
  BackgroundColor PeachPuff
  BorderColor Black
}

skinparam usecase {
  BackgroundColor<< Main >> PeachPuff
  BorderColor<< Main >> Black

  BackgroundColor<< Include >> DarkSeaGreen
  BorderColor<< Include >> Black

  BackgroundColor<< Extend >> CornflowerBlue
  BorderColor<< Extend >> Black
}

skinparam package {
  BackgroundColor Ivory
  BorderColor Gray
}

left to right direction

:User: as RegularUser
:User+: as UserPlus


package App {
  usecase "Sign Up" as UC1 << Main >>
  usecase "Sign In" as UC2 << Main >>
  usecase "Create exsercise" as UC3 << Main >>
  usecase "Setting up exsercise" as UC4 << Main >>
  usecase "Create training plan" as UC5 << Main >>
  usecase "Track the statistic" as UC6 << Main >>
  usecase "Export report" as UC7 << Main >>
  usecase "Make posts" as UC8 << Main >>
  usecase "Read and comment other user`s posts" as UC9 << Main >>
  usecase "Subscribe to other users" as UC10 << Main >>

  (Fill in the register form) as IU1 << Include >>
  (Specify exsercise data) as IU2 << Include >>
  (Fill in personal exsercise params) as IU3 << Include >>
  (Specify order and dates of exsercises) as IU4 << Include >>
  (Specify file format) as IU5 << Include >>
  (Enable/Disable notifications) as IU6 << Include >>

  (Customize profile) as EU1 << Extend >>
  (Add to favorite) as EU2 << Extend >>
  (Choose date range) as EU3 << Extend >>
  (Add photos) as EU4 << Extend >>


  usecase "Make user`s groups" as UC11 << Main >>

  usecase IUP1 << Include >> as "Make personal training plan.
  ..Description..
  It can be exclusive for each added user
  or for the whole group in general with a little
  clarification for each one"

  usecase IUP2 << Include >> as "See the statistics of users in group.
  ..Description..
  There is no need for users in group to export
  their reports because it would be seen
  by the owner of a group"

  (Make multiple groups) as EUP1 << Extend >>
}


UserPlus -left-|> RegularUser #red

RegularUser -- UC1
RegularUser -- UC2
RegularUser -- UC3
RegularUser -- UC4
RegularUser -- UC5
RegularUser -- UC6
RegularUser -- UC7
RegularUser -- UC8
RegularUser -- UC9
RegularUser -- UC10

UC1 ..> IU1 #green;text:green : include
UC3 ..> IU2 #green;text:green : include
UC3 ..> IU3 #green;text:green : include
UC4 ..> IU3 #green;text:green : include
UC5 ..> IU4 #green;text:green : include
UC7 ..> IU5 #green;text:green : include
UC10 ..> IU6 #green;text:green : include

UC1 <.. EU1 #blue;text:blue : extend
UC2 <.. EU1 #blue;text:blue : extend
UC3 <.. EU2 #blue;text:blue : extend
UC4 <.. EU2 #blue;text:blue : extend
UC7 <.. EU3 #blue;text:blue : extend
UC8 <.. EU4 #blue;text:blue : extend

note right of EU1
  Profile customisation won`t be
  massive due to uselessness
  of this ligh-weighted social system.
end note

note right of IU2
  The data of exsercises
  would be common to all, but
  there might be some features
  inherit in specific/exotic exsercises.
  For example, instead of choosing
  weight you can choose time measure
end note

note right of IU6
  Also there should be a whole
  notification system of the App
  that the user will be able to
  set up in settings
end note


UserPlus -- UC11

UC11 ..> IUP1 #green;text:green : include
UC11 ..> IUP2 #green;text:green : include

UC11 <.. EUP1 #blue;text:blue : extend

note right of IUP1
  This also includes making
  personal exsercises
  for users in group.
end note
@enduml
```
