```plantuml
@startuml
skinparam actorStyle awesome

skinparam actor {
  BackgroundColor PeachPuff
  BorderColor Black
}

skinparam usecase {
  BackgroundColor<< Main >> PeachPuff
  BorderColor<< Main >> Black

  BackgroundColor<< Include >> DarkSeaGreen
  BorderColor<< Include >> Black

  BackgroundColor<< Extend >> CornflowerBlue
  BorderColor<< Extend >> Black
}

skinparam package {
  BackgroundColor Ivory
  BorderColor Gray
}

left to right direction

:Пользователь: as user
:Пользователь+: as userPlus


package Приложение {
  usecase "Регистрация" as UC1 << Main >>
  usecase "Вход" as UC2 << Main >>
  usecase "Создание упражнения" as UC3 << Main >>
  usecase "Настройка упражнения" as UC4 << Main >>
  usecase "Создание тренировочного плана" as UC5 << Main >>
  usecase "Отслеживание статистики" as UC6 << Main >>
  usecase "Экспорт отчета" as UC7 << Main >>
  usecase "Создание постов" as UC8 << Main >>
  usecase "Чтение и комментирование постов других пользователей" as UC9 << Main >>
  usecase "Подписка на других пользователей" as UC10 << Main >>

  (Заполнить форму регистрации) as IU1 << Include >>
  (Указать данные упражнения) as IU2 << Include >>
  (Заполнить персональные параметры упражнения) as IU3 << Include >>
  (Указать порядок и даты упражнений) as IU4 << Include >>
  (Указать формат файла) as IU5 << Include >>
  (Включить/Отключить уведомления) as IU6 << Include >>

  (Настроить профиль) as EU1 << Extend >>
  (Добавить в избранное) as EU2 << Extend >>
  (Выбрать диапазон дат) as EU3 << Extend >>
  (Добавить фотографии) as EU4 << Extend >>


  usecase "Создание групп пользователей" as UC11 << Main >>

  usecase IUP1 << Include >> as "Создать персональный тренировочный план.
  ..Описание..
  Он может быть индивидуальным для каждого добавленного пользователя
  или для всей группы в целом с небольшими
  уточнениями для каждого из них"

  usecase IUP2 << Include >> as "Просмотр статистики пользователей в группе.
  ..Описание..
  Пользователям в группе не нужно экспортировать
  свои отчеты, потому что они будут видны
  владельцу группы"

  (Создать несколько групп) as EUP1 << Extend >>
}


userPlus -left-|> user #red

user -- UC1
user -- UC2
user -- UC3
user -- UC4
user -- UC5
user -- UC6
user -- UC7
user -- UC8
user -- UC9
user -- UC10

UC1 ..> IU1 #green;text:green : include
UC3 ..> IU2 #green;text:green : include
UC3 ..> IU3 #green;text:green : include
UC4 ..> IU3 #green;text:green : include
UC5 ..> IU4 #green;text:green : include
UC7 ..> IU5 #green;text:green : include
UC10 ..> IU6 #green;text:green : include

UC1 <.. EU1 #blue;text:blue : extend
UC2 <.. EU1 #blue;text:blue : extend
UC3 <.. EU2 #blue;text:blue : extend
UC4 <.. EU2 #blue;text:blue : extend
UC7 <.. EU3 #blue;text:blue : extend
UC8 <.. EU4 #blue;text:blue : extend

note right of EU1
  Настройка профиля не будет
  масштабной из-за ненужности
  в этой легковесной социальной системе.
end note

note right of IU2
  Данные об упражнениях
  будут общими для всех, но
  могут быть некоторые особенности,
  наследуемые от конкретных/экзотических упражнений.
  Например, вместо выбора веса можно выбрать
  меру времени.
end note

note right of IU6
  Также должна быть вся
  система уведомлений приложения,
  которую пользователь сможет настроить
  в настройках.
end note


userPlus -- UC11

UC11 ..> IUP1 #green;text:green : include
UC11 ..> IUP2 #green;text:green : include

UC11 <.. EUP1 #blue;text:blue : extend

note right of IUP1
  Это также включает создание
  персональных упражнений
  для пользователей в группе.
end note
@enduml

```
